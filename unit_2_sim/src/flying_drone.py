#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from cvg_sim_msgs.msg import Altimeter

class Flying():

    def __init__(self):
        self.hector_vel_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        self.hector_sub = rospy.Subscriber("/altimeter", Altimeter, self.scan_callback)
        
        self.cmd = Twist()
        self.ctrl_c = False
        self.rate = rospy.Rate(10) # 10hz
        self.a = 0.0
        rospy.on_shutdown(self.shutdownhook)

    def scan_callback(self, msg):
        self.a = msg.altitude 
         

    def shutdownhook(self):
        # works better than the rospy.is_shutdown()
        self.ctrl_c = True

    def move_drone(self):
        while not self.ctrl_c:

            if self.a < 3.0:
                self.cmd.linear.z = 0.27
            if self.a > 3.01:
                self.cmd.linear.z = 0.0
            self.cmd.angular.z = 0.04
            self.hector_vel_publisher.publish(self.cmd)


            

if __name__ == '__main__':
    rospy.init_node('hector_test', anonymous=True)
    hector_object = Flying()
    try:
        hector_object.move_drone()
    except rospy.ROSInterruptException:
        pass